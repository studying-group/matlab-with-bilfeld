clc;
U = 12;
L = 6;
R = 3;
K = U / R;
T = L / R;

t = linspace(0, 10, 101);
I = K - K * exp(-t/T);

plot(t, I), grid;