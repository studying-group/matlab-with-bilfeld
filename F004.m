clc;
clear;

U = 12;
L = 6;
R = 3;
K = U / R;
T = L / R;

syms K T t;
x = sym('1');
X = laplace(x);
y = K - K * exp(-t/T);
Y = laplace(y);